import React from 'react';

function RoleInfo(props){
  const role = props.type[0].toUpperCase() + props.type.slice(1);
  const icon = <i className={`icon-${props.type}`} />;
  
  return(
    <>
      <div className="block">
        <div className="block__head">
          <div className="head__h">the {role}</div>
          <div className="head__p">
            You have chosen the role - <span className="text_dark">the {role}</span>
          </div>
          <div className="head__img">
            {icon}
          </div>
        </div>
        <div className="block__bottom">
          <button
            onClick={props.toggleTerms}
            className="btn btn_green">
            {icon}
            Go To Cabinet
          </button>
          <button
            onClick={props.clearType}
            className="btn btn_grey">
            <i className="icon-close" />
            Cancel
          </button>
        </div>
      </div>
      <div className="modal-guide">
        <div className="modal-guide__button">
          <button onClick={props.toggleTerms} className="btn btn_white">
            <i className="icon-guide" /> Guide Flow
          </button>
        </div>
        <div className="modal-guide__description">
          <div className="description__text">
            Study the guide flow for the convenience of using the service.
          </div>
        </div>
      </div>
    </>
  )
}

export default RoleInfo;
