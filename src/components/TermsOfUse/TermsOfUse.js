import React, {useEffect, useState} from 'react';
import loader from '../../img/loader.gif';

function TermsOfUse(props){
  const [termsText, setTermsText] = useState(null);
  const [disabledBtn, setDisabledBtn] = useState(true);
  
  useEffect(() => {
    fetch("https://60c74df306f3160017d29000.mockapi.io/api/v1/tossource")
      .then(promise => promise.json())
      .then(data => setTermsText(data[0].text));
  });
  
  const createMarkup = (str) => {
    return {__html: str};
  };
  
  const handleScroll = (e) => {
    const {scrollTop, clientHeight, scrollHeight} = e.target;
    if (Math.ceil(scrollTop + clientHeight) >= scrollHeight) {
      setDisabledBtn(false);
    }
  };
  
  const Loader = <div className="loader"><img src={loader} alt=""/></div>;
  const disabledClass = disabledBtn ? 'block__bottom_disabled' : '';
  
  return(
    <div className="block">
      <button onClick={props.toggleTerms} className="block__button-close">
        <i className="icon-close" />
      </button>
      <div className="block__head">
        <div className="head__h">Terms & Conditions</div>
        <div className="head__p">
          You should obliged to apply the
          Term & Conditions to yse the service.
        </div>
      </div>
      <div className="block__body">
        <div className="terms" onScroll={handleScroll}>
          {
            termsText
              ? <div dangerouslySetInnerHTML={createMarkup(termsText)} />
              : Loader
          }
        </div>
      </div>
      <div className={`block__bottom ${disabledClass}`}>
        <button
          onClick={props.toggleTerms}
          className="btn btn_green"
          disabled={disabledBtn}>
          <i className="icon-checkmark" />
          I Agree
        </button>
      </div>
    </div>
  )
  
}

export default TermsOfUse;
