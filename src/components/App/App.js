import React from 'react';
import './style.scss';
import Layout from "../Layout/Layout";
import FirstStep from "../FirstStep/FirstStep";
import RoleInfo from "../RoleInfo/RoleInfo";
import TermsOfUse from "../TermsOfUse/TermsOfUse";

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      type: null,
      openedTerms: false,
    };
    this.onBorrower = this.onBorrower.bind(this);
    this.onLender = this.onLender.bind(this);
    this.toggleTerms = this.toggleTerms.bind(this);
    this.clearType = this.clearType.bind(this);
  }
  
  onBorrower() {
    this.setState({type: 'borrower'})
  }
  
  onLender() {
    this.setState({type: 'lender'})
  }
  
  toggleTerms() {
    this.setState({openedTerms: !this.state.openedTerms})
  }
  
  clearType() {
    this.setState({type: null})
  }
  
  render() {
    const {type, openedTerms} = this.state;
    return(
      <Layout>
        {!type && <FirstStep onLender={this.onLender} onBorrower={this.onBorrower} />}
        {
          type 
          && !openedTerms 
          && <RoleInfo 
              type={this.state.type} 
              toggleTerms={this.toggleTerms} 
              clearType={this.clearType}
             />
        }
        {openedTerms && <TermsOfUse toggleTerms={this.toggleTerms} />}
      </Layout>
    )
  }
}

export default App;
