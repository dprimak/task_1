import React, {useState} from 'react';
import logo from "../../img/logo-generic.svg";

function Header(props){
  const [openedMenu, setOpenedMenu] = useState(false);
  
  const toggleMenu = () => {
    setOpenedMenu(!openedMenu);
  };
  
  const openedBtnClass = openedMenu ? 'header__btn-menu_open' : '';
  const openedMenuClass = openedMenu ? 'menu-wrap_open' : '';
  
  return(
    <header className="header">
      <div className="header__logo">
        <a href="#">
          <img className="logo" src={logo} alt=""/>
        </a>
      </div>
      <div className={`menu-wrap ${openedMenuClass}`}>
        <div className="header__navigation">
          <ul className="nav">
            <li className="nav__item">
              <a href="#" className="nav__link">Expertise</a>
            </li>
            <li className="nav__item">
              <a href="#" className="nav__link">Industries</a>
            </li>
            <li className="nav__item">
              <a href="#" className="nav__link">News</a>
            </li>
            <li className="nav__item">
              <a href="#" className="nav__link">Partners</a>
            </li>
            <li className="nav__item">
              <a href="#" className="nav__link">Careers</a>
            </li>
            <li className="nav__item">
              <a href="#" className="nav__link">About us</a>
            </li>
          </ul>
        </div>
        <div className="header__auth">
          <button className="btn btn_dark"><i className="icon-lock" /> Sing In</button>
          <button className="btn btn_green"><i className="icon-signup" /> Sing In</button>
        </div>
      </div>
      <button onClick={toggleMenu} className={`header__btn-menu ${openedBtnClass}`}>
        <span className="btn-menu__line"/>
        <span className="btn-menu__line"/>
        <span className="btn-menu__line"/>
      </button>
    </header>
  )
}

export default Header;
