import React from 'react';
import Header from "./Header";

function Layout(props) {
  return(
    <div className="container">
      <Header />
      <div className="main">
        {props.children}
      </div>
    </div>
  )
}

export default Layout;
