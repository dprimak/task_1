import React from 'react';

function FirstStep(props){
  return(
    <div className="block">
      <div className="block__head">
        <div className="head__h">Choose your role</div>
        <div className="head__p">Please, choose your role in service.</div>
      </div>
      <div className="block__bottom">
        <button onClick={props.onBorrower} className="btn btn_green">
          <i className="icon-borrower" /> 
          The Borrower
        </button>
        <button onClick={props.onLender} className="btn btn_green">
          <i className="icon-lender" />
          The Lender
        </button>
      </div>
    </div>
  )
}

export default FirstStep;
